# Azure AKS Workshop

Welcome to the AKS Workshop. Here is described all steps to go through the workshop process.

## Pre-Requisites

### Install Azure CLI

First, there are a couple of steps prior to start working in the workshop. Azure CLI is required to provision the infrastructure needed for the workshop. Please, follow up this link to get the cli installation processes.

[Install Azure CLI steps](InstallAzureCLI.md)

### Create an Azure Subscription

To use Azure cloud resources, an Azure account and a Subscription is required. Your Endava's account is created already, therefore, you will need to create the Subscription in this account. Access to this link to go throught the creation processes.

[Create Subscription steps](createAzureSubscription.md)


##  ***The next section "Deploy the Kubernetes cluster" will work on the WorkShop, just install the Azure CLI and create an Azure subscription as prerequisites***


## Deploy Kubernetes cluster

Before starting, an Azure Account must have been created and the Azure CLI must have been installed. To create an Azure account, please check this [link](https://gitlab.com/cloud_kube_workshop/azure/blob/master/createAzureSubscription.md) out. To install Azure CLI follow up this [process](https://gitlab.com/cloud_kube_workshop/azure/blob/master/InstallAzureCLI.md). Also, you should have registered with GitLab. Follow this [link](https://gitlab.com/users/sign_in) to register with GitLab if you haven't done it yet.

### Confirm az CLI installation

To confirm az installation, run the following command:

~~~~
az --version
~~~~

Find this line after running the command:

~~~~
azure-cli                         2.0.66
~~~~

### Download AKS Repository

To download Azure repository, use this command:

~~~~
git clone https://gitlab.com/cloud_kube_workshop/azure.git
~~~~

Enter the Azure folder.

### Provision Cluster

Ensure to have the prompt in the Azure folder repository cloned previously.

#### For Windows

If using Windows, open a Powershell Console and execute this command:

~~~~
./createAks.ps1
~~~~

Enter your Endava credentials. Wait until the cluster has been created. This process could take around 15 minutes.

#### For Linux and MacOs

If using Linux or MacOs, open a Terminal Console and execute this command:

~~~~
sudo bash ./createAks.sh
~~~~

Enter your Endava credentials. Wait until the cluster has been created. This process could take around 15 minutes.

In order to manage the cluster, run the following command:

~~~~
sudo chown $(id -u):$(id -g) $HOME/.kube/config
~~~~

## Destroy Cluster

To remove all resources created for this workshop, follow the steps below dependeding the case. Ensure to have the prompt in the Azure folder repository cloned previously.

### For Windows

If using Windows, open a Powershell Console and execute this command:

~~~~
./destroyAks.ps1
~~~~

Enter your Endava credentials and wait upon completion. This process could take around 15 minutes.

### For Linux and MacOs

If using Linux or MacOs, open a Terminal Console and execute this command:

~~~~
bash ./destroyAks.sh
~~~~

Enter your Endava credentials and wait upon completion. This process could take around 15 minutes.