$azlogin=Get-Credential -message ("Enter your Endava email and password")
if ($azlogin -notlike "*@endava.com") {$userName=$azlogin.username + "@endava.com"} else {$userName=$azlogin.username}
if (az login -u $userName -p $azlogin.getnetworkcredential().password){
    $resourceGroup="aksWorkShop"
    write-output "Logon successfully, removing resource group $resourceGroup..."
    $userName=$userName.substring(0,$userName.indexof('@'))
    $servicePrincipal="aks-" + $userName
    az group delete --name $resourceGroup --yes --verbose
    write-output "Deleting service principal $servicePrincipal..."
    $appid=$(az ad sp create-for-rbac -n $servicePrincipal --role contributor --query appId --verbose)
    az ad sp delete --id $appid --verbose
    write-output "AKS Cluster deleted successfully!"
} else {
    write-output "Wrong credentials, review them!"
}